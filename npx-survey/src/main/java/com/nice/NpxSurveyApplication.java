package com.nice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.nice.NpxSurveyApplication;

@SpringBootApplication
public class NpxSurveyApplication {

	public static void main(String[] args) {
		SpringApplication.run(NpxSurveyApplication.class, args);
	}
}