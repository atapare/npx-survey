package com.nice.repository;

import org.springframework.beans.factory.annotation.Autowired;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.nice.model.Provider;
/**
 * Repository to communicate with Provider dynamoDB table
 * @author bhdeshmukh
 *
 */
public class ProviderRepository {

	@Autowired
	private DynamoDBMapper mapper;

	public ProviderRepository(DynamoDBMapper mapper) {
		this.mapper = mapper;
	}
	
	public Provider get(String providerIdfier) {
		return mapper.load(Provider.class, providerIdfier);
	}
	
	public void save(Provider provider) {
		mapper.save(provider);
	}
	
}
