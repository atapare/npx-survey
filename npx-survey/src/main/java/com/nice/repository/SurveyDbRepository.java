package com.nice.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.nice.model.Survey;
/**
 * Repository to communicate with DynamoDB Survey table.
 * @author bhdeshmukh
 *
 */
@Repository
public class SurveyDbRepository {
	private static final Logger LOGGER = LoggerFactory.getLogger(SurveyDbRepository.class);

	@Autowired
	private DynamoDBMapper mapper;

	public SurveyDbRepository(DynamoDBMapper mapper) {
		this.mapper = mapper;
	}
	
	public Survey getSurveyDetails(String surveyIdfier) {
		LOGGER.info("DynamoDB object : " + mapper);
		// this is set up we need to fetch data from DB
		return mapper.load(Survey.class, surveyIdfier);
	}
	
	public void save(Survey survey) {
		mapper.save(survey);
	}

}