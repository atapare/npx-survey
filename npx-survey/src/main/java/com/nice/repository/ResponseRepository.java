package com.nice.repository;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.nice.model.Response;
/**
 * Repository to communicate with Response dynamoDB table
 * @author bhdeshmukh
 *
 */
public class ResponseRepository {
	
	private DynamoDBMapper mapper;

	public ResponseRepository(DynamoDBMapper mapper) {
		this.mapper = mapper;
	}
	
	public Response get(String providerIdfier) {
		return mapper.load(Response.class, providerIdfier);
	}
	
	public void save(Response response) {
		mapper.save(response);
	}
}
