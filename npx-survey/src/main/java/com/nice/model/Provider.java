package com.nice.model;

import java.io.Serializable;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
/**
 * Class representing dynamoDB table for provider
 * @author bhdeshmukh
 *
 */
@DynamoDBTable(tableName = "provider")
public class Provider implements Serializable{

	/**
	 * Default serial version id
	 */
	private static final long serialVersionUID = 1L;
	private String providerIdfier;
	private String status;
	private Map<String,Object> providerFields;
	
	@DynamoDBHashKey
	public String getProviderIdfier() {
		return providerIdfier;
	}
	
	public void setProviderIdfier(String providerIdfier) {
		this.providerIdfier = providerIdfier;
	}
	
	@DynamoDBAttribute
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	@DynamoDBAttribute
	public Map<String, Object> getProviderFields() {
		return providerFields;
	}
	
	public void setProviderFields(Map<String, Object> providerFields) {
		this.providerFields = providerFields;
	}
	
	
}
