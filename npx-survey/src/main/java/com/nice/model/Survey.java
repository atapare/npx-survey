package com.nice.model;

import java.io.Serializable;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

/**
 * Class representing dynamoDB table for survey
 * @author bhdeshmukh
 *
 */
@DynamoDBTable(tableName = "survey")
public class Survey implements Serializable{

	private static final long serialVersionUID = 1L;
	private String surveyIdfier;
	private Map<String,Map<String,Object>> questions;
	
	@DynamoDBHashKey
	public String getSurveyIdfier() {
		return surveyIdfier;
	}
	public void setSurveyIdfier(String surveyIdfier) {
		this.surveyIdfier = surveyIdfier;
	}
	@DynamoDBAttribute
	public Map<String, Map<String, Object>> getQuestions() {
		return questions;
	}
	public void setQuestions(Map<String, Map<String, Object>> questions) {
		this.questions = questions;
	}
	
	
}