package com.nice.model;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverted;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
/**
 * Class representing dynamoDB table for responses
 * @author bhdeshmukh
 *
 */
@DynamoDBTable(tableName = "responses")
public class Response implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(Response.class);
	private String surveyId;
	private Map<String,String> providerDetails;
	private Map<String,Map<String,Object>> responseDetails;
	private String providerIdfier;
	
	
	@DynamoDBHashKey
	public String getProviderIdfier() {
		return this.providerIdfier;
	}
	public void setProviderIdfier(String providerIdfier) {
		this.providerIdfier = providerIdfier;
	}
	
	@DynamoDBAttribute
	public String getSurveyId() {
		return surveyId;
	}
	
	public void setSurveyId(String surveyId) {
		this.surveyId = surveyId;
	}
	
	@DynamoDBAttribute
	public Map<String, String> getProviderDetails() {
		return providerDetails;
	}
	public void setProviderDetails(Map<String, String> providerDetails) {
		this.providerDetails = providerDetails;
	}
	
	@DynamoDBAttribute
	@DynamoDBTypeConverted(converter = ProviderFieldsTypeConverter.class)
	public Map<String, Map<String, Object>> getResponseDetails() {
		return responseDetails;
	}
	public void setResponseDetails(Map<String, Map<String, Object>> responseDetails) {
		this.responseDetails = responseDetails;
	}
	
	/**
	 * Class to marshall and unmarshall dynamodb fields of custom type
	 * Map<String, Object> as DyanmoDB don't understand Object of type T and only
	 * understand String,Number,List or Map
	 * @author devdattd
	 *
	 */
	static public class ProviderFieldsTypeConverter implements DynamoDBTypeConverter<String, Map<String, Object>> {

		@Override
		public String convert(Map<String, Object> providerFieldMap) {

			ObjectMapper objectMapper = new ObjectMapper();
			String providerFieldJson = null;
			try {
				providerFieldJson = objectMapper.writeValueAsString(providerFieldMap);
			} catch (JsonProcessingException e) {
				// TODO  - Adding Loggers, need to decide format, use custom exception, to be recived in Service class saving this Model's entity.
				LOGGER.error("Failed during converting from Map Object of Provider Fields to JSON");
			}
			return providerFieldJson;
		}

		@Override
		public Map<String, Object> unconvert(String providerFieldJson) {
			ObjectMapper objectMapper = new ObjectMapper();
			Map<String, Object> providerFieldMap = null;
			try {
				providerFieldMap = objectMapper.readValue(providerFieldJson, new TypeReference<Map<String, Object>>() {
				});
			} catch (IOException e) {
				// TODO  - Adding Loggers, need to decide format, use custom exception, to be recived in Service class saving this Model's entity.
				LOGGER.error("Failed during converting from JSON to Map Object of Provider Fields");
			}

			return providerFieldMap;
		}
	}

}
