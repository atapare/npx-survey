package com.nice.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.nice.repository.ProviderRepository;
import com.nice.repository.ResponseRepository;
import com.nice.repository.SurveyDbRepository;
import com.nice.service.IProviderService;
import com.nice.service.IResponseService;
import com.nice.service.ISurveyService;
import com.nice.service.impl.ProviderServiceImpl;
import com.nice.service.impl.ResponseServiceImpl;
import com.nice.service.impl.SurveyServiceImpl;

@Configuration
public class DynamoDbConfig {

	@Value("${amazon.access.key}")
	private String awsAccessKey;

	@Value("${amazon.access.secret-key}")
	private String awsSecretKey;

	@Value("${amazon.region}")
	private String awsRegion;

	@Value("${amazon.end-point.url}")
	private String awsDynamoDBEndPoint;
	
	@Bean
	public IResponseService responseService() {
		return new ResponseServiceImpl(responseRepository());
	}
	@Bean
	public IProviderService providerService() {
		return new ProviderServiceImpl(providerRepository());
	}
	@Bean
	public ISurveyService surveyService() {
		return new SurveyServiceImpl(surveyRepository());
	}
	
	@Bean
	public ResponseRepository responseRepository() {
		return new ResponseRepository(mapper());
	}

	@Bean
	public SurveyDbRepository surveyRepository() {
		return new SurveyDbRepository(mapper());
	}
	
	@Bean
	public ProviderRepository providerRepository() {
		return new ProviderRepository(mapper());
	}

	@Bean
	public DynamoDBMapper mapper() {
		return new DynamoDBMapper(amazonDynamoDBConfig());
	}

	public AmazonDynamoDB amazonDynamoDBConfig() {
		return AmazonDynamoDBClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(awsAccessKey, awsSecretKey)))
				.build();
	}
}