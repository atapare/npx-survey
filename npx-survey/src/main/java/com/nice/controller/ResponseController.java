package com.nice.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nice.model.Response;
import com.nice.service.IResponseService;
import org.springframework.web.bind.annotation.RequestMethod;
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*", methods = { RequestMethod.GET, RequestMethod.POST,
        RequestMethod.OPTIONS })
public class ResponseController {
	
	@Autowired
    private IResponseService responseService;
	private static final Logger LOGGER = LoggerFactory.getLogger(ResponseController.class);
	
	@PostMapping("/responses")
	public void saveResponse(@RequestBody Response response) {
		LOGGER.info("Saving responses......");
		String providerIdfier=""+System.currentTimeMillis();
		response.setProviderIdfier(providerIdfier);
		responseService.saveResponses(response);
	}
	
	@GetMapping(path="/responses",produces = MediaType.APPLICATION_JSON_VALUE)
	public Response getResponses(@RequestParam String providerIdfier) {
		LOGGER.info("Retrieving responses......");
		Response response = responseService.getResponses(providerIdfier);
		return response;
	}

}
