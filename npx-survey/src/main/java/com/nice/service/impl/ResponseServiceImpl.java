package com.nice.service.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.nice.model.Response;
import com.nice.repository.ResponseRepository;
import com.nice.service.IResponseService;
/**
 * Class for doing various reponse operations
 * @author bhdeshmukh
 *
 */
public class ResponseServiceImpl implements IResponseService {
	
	@Autowired
	private ResponseRepository responseRepository;

	public ResponseServiceImpl(ResponseRepository responseRepository) {
		this.responseRepository = responseRepository;
	}

	@Override
	public Response getResponses(String providerIdfier) {
		return responseRepository.get(providerIdfier);
	}

	@Override
	public void saveResponses(Response response) {
		responseRepository.save(response);		
	}
	
	
}
