package com.nice.service.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.nice.model.Survey;
import com.nice.repository.SurveyDbRepository;
import com.nice.service.ISurveyService;
/**
 * Class for doing various survey operations
 * @author bhdeshmukh
 *
 */
public class SurveyServiceImpl implements ISurveyService{

	@Autowired
	private SurveyDbRepository surveyRepository;
	
	public SurveyServiceImpl(SurveyDbRepository surveyRepository) {
		this.surveyRepository = surveyRepository;
	}

	@Override
	public Survey getSurvey(String surveyIdfier) {
		return surveyRepository.getSurveyDetails(surveyIdfier);
	}

	@Override
	public void saveSurvey(Survey survey) {
		surveyRepository.save(survey);
	}

}
