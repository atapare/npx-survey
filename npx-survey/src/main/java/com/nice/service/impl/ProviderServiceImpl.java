package com.nice.service.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.nice.model.Provider;
import com.nice.repository.ProviderRepository;
import com.nice.service.IProviderService;
/**
 * Class for doing various provider operations 
 * @author bhdeshmukh
 *
 */
public class ProviderServiceImpl implements IProviderService{
	
	@Autowired
	private ProviderRepository providerRepository;
	
	public ProviderServiceImpl(ProviderRepository providerRepository) {
		this.providerRepository = providerRepository;
	}

	@Override
	public Provider getProvider(String providerIdfier) {
		providerRepository.get(providerIdfier);
		return null;
	}

	@Override
	public void saveProvider(Provider provider) {
		providerRepository.save(provider);		
	}

}
