package com.nice.service;

import com.nice.model.Provider;

/**
 * Interface to represent contract for provider
 * @author bhdeshmukh
 *
 */
public interface IProviderService {

	public Provider getProvider(String providerIdfier);
	
	public void saveProvider(Provider provider);
}
