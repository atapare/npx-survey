package com.nice.service;

import com.nice.model.Survey;

/**
 * Interface to represent contract for survey
 * @author bhdeshmukh
 *
 */
public interface ISurveyService {

	public Survey getSurvey(String surveyIdfier);
	
	public void saveSurvey(Survey survey);
}
