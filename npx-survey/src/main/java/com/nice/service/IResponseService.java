package com.nice.service;

import com.nice.model.Response;

/**
 * Interface to represent contract for response
 * @author bhdeshmukh
 *
 */
public interface IResponseService {
	
	public Response getResponses(String providerIdfier);
	
	public void saveResponses(Response response);
}
