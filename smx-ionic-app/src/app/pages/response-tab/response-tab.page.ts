import { Component } from '@angular/core';

@Component({
  selector: 'app-response-tab',
  templateUrl: 'response-tab.page.html',
  styleUrls: ['response-tab.page.scss']
})
export class ResponseTabPage {

  goodRespones: any = [];
  avgRespones: any = [];
  showlistFlag: boolean = true;

  constructor() {
    this.goodRespones = [{
      providerDetails: {
        fname: 'Shekhar',
        lname: 'shetty',
        email: 'ss@test.com'
      },
      responseDetails: {
        primaryScore: { value: 9 },
        primaryComment: { value: 'It was very good experience' },
        answers: {
          dataAttrCode: '6',
          answer: '9'
        }
      },
      providerIdfier: '1594648102958'
    }, {
      providerDetails: {
        fname: 'Aditya',
        lname: 'Gupta',
        email: 'ag@test.com'
      },
      responseDetails: {
        primaryScore: { value: 10 },
        primaryComment: { value: 'Nice product and its service' },
        answers: {
          dataAttrCode: '6',
          answer: '7'
        }
      },
      providerIdfier: '1594648102958'
    }, {
      providerDetails: {
        fname: 'Mahip',
        lname: 'Jain',
        email: 'mj@test.com'
      },
      responseDetails: {
        primaryScore: { value: 7 },
        primaryComment: { value: 'Good' },
        answers: {
          dataAttrCode: '6',
          answer: '7'
        }
      },
      providerIdfier: '1594648102958'
    }];

    this.avgRespones = [{
      providerDetails: {
        fname: 'ananda',
        lname: 'tapare',
        email: 'at@test.com'
      },
      responseDetails: {
        primaryScore: { value: 4 },
        primaryComment: { value: 'need improvement product service' },
        answers: {
          dataAttrCode: '6',
          answer: '9'
        }
      },
      providerIdfier: '1594648102958'
    }, {
      providerDetails: {
        fname: 'bharti',
        lname: 'Deshmukh',
        email: 'bd@test.com'
      },
      responseDetails: {
        primaryScore: { value: 5 },
        primaryComment: { value: 'product services not upto expectation' },
        answers: {
          dataAttrCode: '6',
          answer: '7'
        }
      },
      providerIdfier: '1594648102958'
    }, {
      providerDetails: {
        fname: 'Akshay',
        lname: 'Rathod',
        email: 'ar@test.com'
      },
      responseDetails: {
        primaryScore: { value: 2 },
        primaryComment: { value: 'Bad' },
        answers: {
          dataAttrCode: '6',
          answer: '7'
        }
      },
      providerIdfier: '1594648102958'
    }];

  }

}
